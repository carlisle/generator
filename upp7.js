"use strict";

// ECMAScript6 features used:
// class 
// extend of Array
// const
// let

function Attribute ( new_name, new_value ) {
  this.name  = new_name;
  this.value = new_value;
}

Attribute.prototype.toString = function () {
  return ( this.value );
}

Attribute.prototype.verbose = function () {
  return ( this.name + " : " + this.value );
}

Attribute.prototype.modify = function ( new_value ) {
  this.value = new_value;
}

// ------------------------------------------------------------------

class AttributeArray extends Array {

  constructor(len) {
    super(len);
  }

  total () {
    var total = 0;
    
    for ( let i=0; i < this.length; i+=1 )
      total += this[i].value;
    return total;
  }

}
    
// ------------------------------------------------------------------

class UppArray extends AttributeArray {

  constructor(len) {
    super(len);
  }

  generate2d6 () {

    for (let i=Strength; i <= SocialStatus; i++ ) {
      this[i].modify( roll2d6() );
    }
  return;
  }

  generateBest2of3d6 () {

    for (let i=Strength; i <= SocialStatus; i++ ) {
      this[i].modify( best2of3d6() );
    }
    return;
  }

  generatePsi () {
// 2d6 - terms
    this[PsionicStrength].modify( roll2d6() );
  }

  reset () {

    for ( let i=0; i<this.length; i+=1 ) {
    this[i].modify( 0 );
    }
  return;
  }

  display () {

    var output = "";

    for ( let i = Strength; i <= SocialStatus; i++ ) {
      output = output + this[i].display();
    } 
    if ( this[PsionicStrength] != 0 ) {
      output = output + '-' + this[PsionicStrength].display();
    }
  return output;
  }

  nobleTitle () {

    var _title = '#';
    var _status = this[SocialStatus].value;

    if ( _status < 10 )
      _title = 'None';

    else switch (_status) {

      case 10:
        _title = 'Lord/Lady';
        break;
      case 11:
        _title = 'Knight/Knightess/Dame';
        break;
      case 12:
        _title = 'Baron/Baroness/Baronet';
        break;
      case 13:
        _title = 'Marquis/Marquesa/Marchioness';
        break;
      case 14:
        _title = 'Count/Countess';
        break;
      case 15:
        _title = 'Duke/Duchess';
        break;
      case 16:
        _title = 'Archduke/Archduchess';
        break;
      case 17:
        _title = 'Crown Prince/Crown Princess';
        break;
      case 18:
        _title = 'Emperor/Empress';
        break;
     
      default:
        _title = '!';
    }

    return _title;
  }

}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Characteristic.prototype = new Attribute();        // inheritance 
 
Characteristic.prototype.constructor=Characteristic; // constructor 

function Characteristic ( characteristic_name, chracteristic_value ) { 
  this.name  = characteristic_name;
  this.value = chracteristic_value; 
} 

Characteristic.prototype.toString = function() { 
	return ( this.display() );
}

Characteristic.prototype.verbose = function() {
        return ( this.name + " : " + this.display() );
}

Characteristic.prototype.generate2d6 = function() {

    this.modify( roll2d6() );
}

Characteristic.prototype.generateBest2of3d6 = function() {

    this.modify( best2of3d6() );
}

Characteristic.prototype.display = function () {
// Characteristics are displayed in pseudo-hexadecimal

  var char = this.value;
  var _output = '#';

  if (char < 0 || char > 15 )
    _output = '?';
  else if ( char < 10 )
    _output = char;
  else switch (char) {

    case 10:
        _output = 'A';
        break;
    case 11:
        _output = 'B';
        break;
    case 12:
        _output = 'C';
        break;
    case 13:
        _output = 'D';
        break;
    case 14:
        _output = 'E';
        break;
    case 15:
        _output = 'F';
        break;
    case 16:
        _output = 'G';
        break;
    case 17:
        _output = 'H';
        break;
    case 18:
        _output = 'J';
        break;
    case 19:
        _output = 'K';
        break;
    case 20:
        _output = 'L';
        break;
    case 21:
        _output = 'M';
        break;
    case 22:
        _output = 'N';
        break;
    case 23:
        _output = 'P';
        break;
    case 24:
        _output = 'Q';
        break;
    case 25:
        _output = 'R';
        break;
    case 26:
        _output = 'S';
        break;
    case 27:
        _output = 'T';
        break;
    case 28:
        _output = 'U';
        break;
    case 29:
        _output = 'V';
        break;
    case 30:
        _output = 'W';
        break;
    case 31:
        _output = 'X';
        break;
    case 32:
        _output = 'Y';
        break;
    case 33:
        _output = 'Z';
        break;

    default:
        _output = '!';
  }
  return _output
}

//---------------------------------------------------------------------

// UPP Constants: full name and 3 letter short name

const Strength = 0;
const Str = 0;
const Dexterity = 1;
const Dex = 1;
const Endurance = 2;
const End = 2;
const Intelligence = 3;
const Int = 3;
const Education = 4;
const Edu = 4;
const SocialStatus = 5;
const Soc = 5;
const PsionicStrength = 6;
const Psi = 6;

var UPP = new UppArray();

UPP[Str] = new Characteristic ( 'Strength', 0 );
UPP[Dex] = new Characteristic ( 'Dexterity', 0 );
UPP[End] = new Characteristic ( 'Endurance', 0 );
UPP[Int] = new Characteristic ( 'Intellegence', 0 )
UPP[Edu] = new Characteristic ( 'Education'   , 0 );
UPP[Soc] = new Characteristic ( 'Social Status', 0 );
UPP[Psi] = new Characteristic ( 'Psionic Strength', 0 );

// alertThing(UPP);

//alert ( "UPP: " + JSON.stringify(UPP) + " : " + typeof UPP );

//alert ( "UPP: " + JSON.stringify(UPP[0]) + " : " + typeof UPP[0] );
