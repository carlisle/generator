//
// "item_util.js"
// Copyright 2003, Takashi Miyamoto; GPL v2 Licensed.
// http://www.imasy.or.jp/~miyamoto/rpg/javascript/tools_usage.html
// https://web.archive.org/web/20041119125559/http://homepage.mac.com/takashi_miyamoto/rpg/javascript/tools_usage.html
// Reqired: rpg_util.js (thus also util.js is required)
//
// Item
function Item(aDesc, aPrice) {
  this.desc = aDesc;
  this.price = aPrice;
}
Item.prototype.toString = function() {
  return "Item("+this.desc+", "+this.price+")";
}
Item.prototype.toDisp = function() {
  return this.desc+" ("+this.price+"gp)";
}
Item.prototype.toShort = function() {
  var i = this.desc.indexOf(":");
  if (i<0) {
    return this.toString();
  } else {
    return (this.desc.substr(0,i)+"("+this.price+"gp)");
  }
}
// NONE
var NONE = new Item("None", 0);
// Item template
// Method:
// Item getValue() = generate Item from ItemTemplate
//   return new Item(desc(), price());
// toString() : "IT: desc="+desc+" price="+price
function IT(fDesc, fPrice) {
  this.desc = fDesc;
  this.price = fPrice;
}
IT.prototype.toString = function() {
  return "IT("+this.desc+", "+this.price+")";
}
IT.prototype.getValue = function() {
  return new Item(getValue(this.desc), getValue(this.price));
}
// [Adj] + Noun
function AN(anAry) {
  this.ary = anAry;
}
AN.prototype.getValue = function() {
  var len = this.ary.length;
  return this.ary[dice(len-1)-1]+" "+this.ary[len-1];
}
AN.prototype.toString = function() {
  return "AN("+this.ary+")";
}
//
function toDispItem(anAry) {
  if (anAry==null) {return "";}
  var str = "";
  if (isArray(anAry)) {
    for(var i=0; i<anAry.length; i++) {
	  str += (toDispItem(anAry[i])+" ");
	}
  } else {
    str += anAry.toDisp();
  }
  return str;
}

