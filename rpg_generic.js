//function roll1d6 () {return dice(6);   };
//function roll2d6 () {return nDice(2,6);};
//function roll3d6 () {return nDice(3,6);};

function best2of3d6 () {

  var diceList = [ roll1d6(), roll1d6(), roll1d6() ];

  diceList.sort(function(a, b){return a-b});

  return ( diceList[1] + diceList[2] );
}

function worst2of3d6 () {

  var diceList = [ roll1d6(), roll1d6(), roll1d6() ];

  diceList.sort(function(a, b){return a-b});

  return ( diceList[2] + diceList[3] );
}

function roll1d6 ( mod ) {

  var modifier = 0;

  if ( mod === undefined ) modifier = 0;
  else modifier = mod;

  return ( dice(6) + modifier );   
};

function roll2d6 ( mod ) {

  var modifier = 0;

  if ( mod === undefined ) modifier = 0;
  else modifier = mod;

  return ( nDice(2,6) + modifier );
};

function roll3d6 ( mod ) {

  var modifier = 0;

  if ( mod === undefined ) modifier = 0;
  else modifier = mod;

  return ( nDice(3,6) + modifier );
};

function roll4d6 ( mod ) {

  var modifier = 0;

  if ( mod === undefined ) modifier = 0;
  else modifier = mod;

  return ( nDice(4,6) + modifier );
};

function roll5d6 ( mod ) {

  var modifier = 0;

  if ( mod === undefined ) modifier = 0;
  else modifier = mod;

  return ( nDice(5,6) + modifier );
};

function NoNegatives( _number ) {
  return Math.max( 0, _number );
}

function OnlyPositives ( _number ) {
  return Math.max( 1, _number );
};

function alertThing (_thing) {

  alert ( "Thing: " + JSON.stringify(_thing) );
  return true;
};

function writeDivAlign ( DivId, Input, Align ) {

  var _id  = document.getElementById( DivId );
  var _div = document.createElement ("div");

  _div.style.textAlign=Align;
  _div.id = DivId;
  _div.appendChild( document.createTextNode( Input ));

  _id.parentNode.replaceChild( _div , _id );

  return;
}

function appendDivAlign ( DivId, Input, Align ) {

  var _id  = document.getElementById( DivId );
  var _div = document.createElement ("div");

  _div.style.textAlign=Align;
  _div.id = DivId;
  _div.appendChild( document.createTextNode( Input ));

  _id.appendChild( _div );

  return;
}
