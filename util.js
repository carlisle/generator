//
// util.js : Utility Functions
// Copyright 2003, Takashi Miyamoto; GPL v2 Licensed.
// http://www.imasy.or.jp/~miyamoto/rpg/javascript/tools_usage.html
// https://web.archive.org/web/20041119125559/http://homepage.mac.com/takashi_miyamoto/rpg/javascript/tools_usage.html
//
// for Debug print
var STDOUT = document; // Destination for debug print
var DEBUG = false; // Flag for print
function debugln(s) {
  if(DEBUG) {STDOUT.writeln(s+"<BR>");}
}
function debug(s) {
  if(DEBUG) {STDOUT.writeln(s);}
}
// Object type
function isObject(o) {
  return (typeof(o)=="object");
}
function isArray(o) {
  return (isObject(o) && (o.length) &&(!isString(o)));
}
function isFunction(o) {
  return (typeof(o)=="function");
}
function isString(o) {
  return (typeof(o)=="string");
}
// getArgs
function getArgs() {
  var args = new Object();
  var argstr = location.search.substring(1);
  var params = argstr.split("&");
  for(var i=0; i<params.length; i++) {
    var p = params[i].indexOf("=");
	if (p<0) {continue;}
	var lhs = params[i].substring(0,p);
	var rhs = params[i].substring(p+1);
	args[lhs] = unescape(rhs);
  }
  return args;
}
