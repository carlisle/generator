//
// "rpg_util.js"
// Copyright 2003, Takashi Miyamoto; GPL v2 Licensed.
// http://www.imasy.or.jp/~miyamoto/rpg/javascript/tools_usage.html
// https://web.archive.org/web/20041119125559/http://homepage.mac.com/takashi_miyamoto/rpg/javascript/tools_usage.html
// Required: util.js
//
// Dice roll
function dice(nS) {
  return Math.floor(nS*Math.random()+1);
}
function nDice(nD, nS) {
  var sum = 0;
  for(var i=0; i<nD; i++) {sum += dice(nS);}
  return sum;
}
var D2 = function() {return dice(2);};
var D3 = function() {return dice(3);};
var D4 = function() {return dice(4);};
var D6 = function() {return dice(6);};
var D8 = function() {return dice(8);};
var D10 = function() {return dice(10);};
var D12 = function() {return dice(12);};
var D20 = function() {return dice(20);};
var D100 = function() {return dice(100);};
// getValue
function getValue(o) {
  if (o == null) {return null;}
  if (isArray(o)) {
    if (o.length==0) {return null;}
    var ary = new Array();
    var j = 0;
    for(var i=0; i<o.length; i++) {
      var oi = o[i];
      if (oi != null) {
        ary[j] = getValue(oi);
        j++;
      }
    }
    return ary;
  } else {
    if (isFunction(o)) {return o();}
    if (isObject(o)&&(o.getValue)) {return o.getValue();}
    return o;
  }
}
// Random Table Entry
function RTE(aThre, aResult) {
  this.thre = aThre;
  this.result = aResult;
}
RTE.prototype.getValue = function() {
//  debugln("RTE.getValue()"+this);
  return getValue(this.result);
}
RTE.prototype.isChosen = function(x) {
  return (x<=this.thre);
}
RTE.prototype.toString = function() {
  return "RTE("+this.thre+", "+this.result+")";
}
// Class RT(fDice, anArrayOfRTE) (= RandomTable)
// Attribute:
// dice : if Function, dice() is used to generate random number; if integer number, Function dice(n) is used
// ary : an Array of RTE 
// Method:
// roll() : getValue(dice); if dice is Function, dice() is used.
// Function getValue() = return randomly chosen entry.
//   x = roll(); for(i=0; i<ary.length;i++) {if (ary[i].isChosen(x)) returns getValue(ary[i]);}
// toString : "RT: dice="+dice+" entries="entries
//
function RT(fDice, anArrayOfRTE) {
  this.dice = fDice;
  this.ary = anArrayOfRTE;
}
RT.prototype.roll = function() {
  var d = getValue(this.dice);
//  debugln("RT.roll()="+d);
  return d;
}
RT.prototype.getValue = function() {
//  debugln("RT.getValue()"+this);
  var x = this.roll();
  for(var i=0; i<this.ary.length; i++) {
    var rte = this.ary[i];
    if (rte.isChosen(x)) {return getValue(rte);}
  }
  return null;
}
RT.prototype.toString = function() {
  return "RT("+this.dice+", "+this.ary+")";
}
// Choice
function CH(anArray) {
  this.ary = anArray;
}
CH.prototype.getValue = function() {
  if (this.ary.length==0) {return null;}
  var i = dice(this.ary.length)-1;
  return getValue(this.ary[i]);
}
CH.prototype.toString = function() {
  return "CH("+this.ary+")";
}
// Concat Array
function CA(anArray, aSep) {
  this.ary = anArray;
  this.sep = aSep;
}
CA.prototype.getValue = function() {
  if (this.ary.length==0) {return null;}
  var res = "";
  for(var i=0; i<this.ary.length; i++) {
    var val = getValue(this.ary[i]);
    res += val;
    if (i != (this.ary.length-1)) {res += this.sep;}
  }
  return res;
}
CA.prototype.toString = function() {
  return "CA("+this.ary+", \""+this.sep+"\")";
}
// Array Maker
function AM(fNum, anObj) {
  this.num = fNum;
  this.obj = anObj;
}
AM.prototype.getValue = function() {
  var n = getValue(this.num);
  if (n <= 0) {return null;}
  var ary = new Array();
  for(var i=0; i<n; i++) {
    ary[i] = getValue(this.obj);
  }
  return ary;
}
AM.prototype.toString = function() {
  return "AM("+this.num+", "+this.obj+")";
}
//
// for each item of anAry, operate func(item), and concat with sep
//
function opAry(anAry, func, sep) {
  var ret = "";
  if (anAry==null) {return "";}
  switch(anAry.length) {
    case 0:
	  return "";
	case 1:
	  return func(anAry[0]);
	default:
	  for(var i=0;i<anAry.length-1;i++) {
	    ret += func(anAry[i]);
		ret += sep;
	  }
	  ret += func(anAry[anAry.length-1]);
	  return ret;
  }
}
